package com.example.restapiii.Controller;

import com.example.restapiii.Service.AccountService;
import com.example.restapiii.Entity.AccountEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("Account")
public class AccountController {

    @Autowired
    AccountService accountService;
    @GetMapping("/{UserName}/{Password}")
    public AccountEntity getAccount(@PathVariable String UserName, @PathVariable String Password) {
        return accountService.getAccount(UserName, Password);
    }


}
