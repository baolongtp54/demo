package com.example.restapiii.Controller;

import com.example.restapiii.Entity.Order;
import com.example.restapiii.Service.OrderService;
import com.example.restapiii.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("Order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping("/GetByAccountID")
    public List<Order> getAllByAccountId(@RequestParam("id")int id){
        return orderService.getAllOrderByAccountId(id);
    }

    @PostMapping("/orderProduct")
    public HttpStatus addOrder(@RequestParam("accId") int accountId,
                               @RequestParam("proId") int productId,
                               @RequestParam("quantity") int quantity){

        try{
            int resutl = orderService.addOrder(accountId,productId,quantity);
            if(resutl > 0){
                return HttpStatus.OK;
            } else return HttpStatus.NOT_IMPLEMENTED;
        } catch(Exception e) {
            return HttpStatus.BAD_REQUEST;
        }
    }
}
