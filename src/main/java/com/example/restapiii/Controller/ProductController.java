package com.example.restapiii.Controller;

import com.example.restapiii.Entity.ProductEntity;
import com.example.restapiii.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("Product")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/getAll")
    public List<ProductEntity> getAll(){
        return productService.getAll();
    }

    @GetMapping("/getProductById")
    public ProductEntity getById(@RequestParam("id") int id){
        return productService.getProductByID(id);
    }

    @PostMapping("/addProduct")
    public HttpStatus addProduct(@RequestParam("name") String name,
                                 @RequestParam("price") int UnitPrice,
                                 @RequestParam("instock") int unitInStock,
                                 @RequestParam("des") String description,
                                 @RequestParam("manu") String manufacturer,
                                 @RequestParam("con") String condition,
                                 @RequestParam(value = "img", required = false) String imageLink,
                                 @RequestParam(value = "cat") String category){
        try{
            int resutl = productService.addProduct(name, UnitPrice, unitInStock, description, manufacturer, condition, imageLink, category);
            if (resutl == 1){
                return HttpStatus.OK;
            } else return HttpStatus.NOT_IMPLEMENTED;
        }catch(Exception e){

            return HttpStatus.BAD_REQUEST;
        }


    }
}
