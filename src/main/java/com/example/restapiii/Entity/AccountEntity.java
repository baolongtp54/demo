package com.example.restapiii.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Account")
public class AccountEntity {
    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "UserName")
    private String userName;

    @Column(name = "Password")
    private String password;
}
