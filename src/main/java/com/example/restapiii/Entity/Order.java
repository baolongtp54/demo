package com.example.restapiii.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Order")
public class Order {


    @Id
    @Column(name = "ID")
    private int Id;

    @Column(name = "AccountId")
    private int accountId;

    @Column(name = "ProductId")
    private int productId;

    @Column(name = "Quantity")
    private int quantity;
}
