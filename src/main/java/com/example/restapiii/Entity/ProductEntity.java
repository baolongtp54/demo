package com.example.restapiii.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Product")
public class ProductEntity {
    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "Name")
    private String name;

    @Column(name = "UnitPrice")
    private int unitPrice;

    @Column(name = "UnitInStock")
    private int UnitInStock;

    @Column(name = "Description")
    private String description;

    @Column(name = "Manufacturer")
    private String manufacturer;

    @Column(name = "Condition")
    private String condition;

    @Column(name = "ImageLink")
    private String imageLink;

    @Column(name = "Category")
    private String category;

}
