package com.example.restapiii.Repository;

import com.example.restapiii.Entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {


    public AccountEntity getAccountEntitiesByID(int ID);

    @Query(value = "SELECT * FROM Account WHERE UserName = ?1 AND Password = ?2", nativeQuery = true)
    public AccountEntity getAccount(String userName, String password);


}
