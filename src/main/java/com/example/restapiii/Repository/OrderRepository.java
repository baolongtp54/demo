package com.example.restapiii.Repository;

import com.example.restapiii.Entity.Order;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

    @Query(value = "SELECT * FROM [Order] WHERE AccountId = ?1", nativeQuery = true)
    public List<Order> getAllOrderByAccountId(int id);

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO [Order] VALUES (?1, ?2, ?3)", nativeQuery = true)
    public int addOrder(int accountId, int productId, int quantity);

    @Modifying
    @Transactional
    @Query(value = "UPDATE [Order] SET quantity = ?1 WHERE ID = ?2", nativeQuery = true)
    public int updateQuantityById(int quantity, int id);

    @Query(value = "SELECT * FROM [Order] WHERE ID = ?1", nativeQuery = true)
    public Order getOrderById(int id);

    @Query(value =  "SELECT * FROM [Order] WHERE AccountID = ?1 AND ProductID = ?2", nativeQuery = true)
    public Order getOrderByAccountIdAndProductId(int accountId, int productId);
}
