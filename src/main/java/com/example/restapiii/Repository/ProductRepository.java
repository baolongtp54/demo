package com.example.restapiii.Repository;

import com.example.restapiii.Entity.ProductEntity;
import jakarta.transaction.Transactional;
import lombok.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {

    public ProductEntity getProductEntityByID(int id);

    public List<ProductEntity> getProductEntitiesByNameContains(String name);

    @Query(value = "SELECT * FROM Product", nativeQuery = true)
    public List<ProductEntity> getAll();

    @Modifying
    @Transactional
    @Query(value = "INSERT INTO Product VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)", nativeQuery = true)
    public int addProduct(String name, int unitInPrice, int unitInStock, String description, String manufacturer, String condition, String imgLink , String category);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Product SET UnitInStock = ?1 WHERE ID = ?2", nativeQuery = true)
    public int updateProductUnitInStock(int quantity, int id );
}
