package com.example.restapiii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiiiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestApiiiApplication.class, args);
    }

}
