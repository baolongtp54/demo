package com.example.restapiii.Service;

import com.example.restapiii.Entity.AccountEntity;
import com.example.restapiii.Repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    AccountRepository accountRepository;

    public AccountEntity getAccount(String UserName, String Password){
        return accountRepository.getAccount(UserName, Password);
    }
}
