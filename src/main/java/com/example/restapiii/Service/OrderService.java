package com.example.restapiii.Service;

import com.example.restapiii.Entity.Order;
import com.example.restapiii.Entity.ProductEntity;
import com.example.restapiii.Repository.OrderRepository;
import com.example.restapiii.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;

    public List<Order> getAllOrderByAccountId(int id){
        return orderRepository.getAllOrderByAccountId(id);
    }

    public int addOrder(int accountId, int productId, int quantity){
        ProductEntity product = productRepository.getProductEntityByID(productId);
        if (product.getUnitInStock() < quantity){
            return 0;
        } else {
            productRepository.updateProductUnitInStock(product.getUnitInStock()-quantity, productId);
            Order order = null;
            order = orderRepository.getOrderByAccountIdAndProductId(accountId, productId);
            if (order != null){
                return orderRepository.updateQuantityById(order.getQuantity()+ quantity,order.getId());
            } else {
                return orderRepository.addOrder(accountId, productId, quantity);
            }
        }
    }
}
