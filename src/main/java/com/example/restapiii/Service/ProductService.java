package com.example.restapiii.Service;

import com.example.restapiii.Entity.ProductEntity;
import com.example.restapiii.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository repository;

    public List<ProductEntity> getAll(){
        return repository.getAll();
    }

    public ProductEntity getProductByID(int id){
        return repository.getProductEntityByID(id);
    }

    public List<ProductEntity> getProductByNameContain(String name){
        return repository.getProductEntitiesByNameContains(name);
    }

    public int addProduct(String name, int unitInPrice, int unitInStock, String description, String manufacturer, String condition, String imgLink, String category){
        return repository.addProduct(name , unitInPrice, unitInStock, description, manufacturer, condition, imgLink, category);
    }
}
